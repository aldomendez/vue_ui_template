import {Machine, assign}  from 'xstate'

export const fetchMachine = Machine({
    id: 'fetch',
    initial: 'idle',
    context:{
        result: undefined,
        message: undefined,
    },
    states:{
        idle:{
            on: {
                FETCH:'pending'
            }
        },
        pending:{
            on:{
                RESOLVE: {target:'successful', actions:['setResults']},
                REJECT: {target:'failed', actions:['setMessage']},
            }
        },
        failed:{
            on:{
                FETCH:'pending',
            }
        },
        successful:{
            on:{
                FETCH:'pending'
            }
        },
    }
},{
    actions:{
        setResults: assign((ctx, event) => ({
            results: event.results
        })),
        setMessage: assign((ctx, event) => ({
            message: event.message
        }))
    },
})