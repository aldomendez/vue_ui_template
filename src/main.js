import Vue from 'vue'
import App from './App.vue'
import store from './store'
import router from './router'
import axios from 'axios'
import banner from '@/novalink_banner'

import '@/variables.scss'

import './main.css'

if (process.env.NODE_ENV === 'production') {
  console.log(banner)
} else {
  Vue.config.performance = true;
}

axios.defaults.baseURL = process.env.NODE_ENV === 'production'
  ? '/'
  : 'http://nova-portal.test/'

axios.defaults.headers.common = {
  'X-Requested-With': 'XMLHttpRequest',
  'Accept': 'application/json'
}

axios.interceptors.response.use(function (response) {
  return response;
}, function (error) {
  console.log(error.response.status)
  if (error.response.status == 401) {
    store.dispatch('user/destroyToken')
  }
  return error;
});


// window.onresize = postScreenSize
// postScreenSize()
window.onerror = (message, source, lineno, colno, error)=>{
  console.log({message, source, lineno, colno, error})
  // axios.post('api/stats/errors',)
}
// axios.defaults.headers.post['Content-Type'] = 'application/json'

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
