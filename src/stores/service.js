import axios from 'axios'
import { get } from 'lodash'


// -- [ State ] -------------------------------------
let state = {
  stores: {},
}

// -- [ Getters ] -------------------------------------
let getters = {
  companyItems: (state) => {
    return (id) => {
      return get(state.items, id, [])
    }
  },
  item: (state) => {
    return (id) => {
      return get(state.item, id, {})
    }
  },
  itemLoaded: (state) => (item) => state.loaded.items.includes(item),
  clientLoaded: (state) => (client) => state.loaded.clients.includes(client),
}
// -- [ Mutations ] -------------------------------------
let mutations = {
  clearErrors(state, { model }) {
    state.errors[model] = null
  },
  errors(state, { model, errors }) {
    state.errors[model] = errors
  },
  dictionary(state, { id, value }) {
    state[id] = value
  }
}

let apiFactory = (endpoint) => {
  return {
    index() { return axios.get(`pix/${endpoint}`) },
    store(payload) { return axios.post(`pix/${endpoint}`, payload) },
    update(payload) { return axios.put(`pix/${endpoint}/${payload.id}`, payload) },
    destroy(id) { return axios.delete(`pix/${endpoint}/${id}`) }
  }
}

let api = {
  shipment: apiFactory('shipment'),
  brokerAgency: apiFactory('broker_agency'),
  driver: apiFactory('driver'),
  freightBox: apiFactory('freight_box'),
  carrier: apiFactory('carrier'),
  invoiceType: apiFactory('invoice_type'),
  company: apiFactory('company'),
}

// -- [ Actions ] -------------------------------------
let actions = {
  async fetchAllModels({ commit }) {
    Object.keys(api).map((ap) => api[ap].index().then(v => {
      commit('dictionary', { value: v.data, id: ap })
    }).catch(r => {
      commit('errors', r.response.data.errors)
    }))
  },
  async fetch({ commit }, model) {
    api[model].index().then(d => { commit('dictionary', { value: d.data, id: model }) })
  },
  async saveChanges({ dispatch }, { model, payload }) {
    return api[model].update(payload).then(d => {
      dispatch('fetch', model)
      return d
    })
  },
  async saveNew({ dispatch }, { model, payload }) {
    return api[model].store(payload).then(d => {
      dispatch('fetch', model)
      return d
    })
  },
  async destroy({ dispatch, commit }, { model, payload }) {
    return api[model].destroy(payload.id).then(d => {
      dispatch('fetch', model)
      return d
    }).catch(r => {
      commit('errors', { model, errors: r.response.data.errors })
    })
  },
  async fetchUnitsOfMeasure({ commit, }, { item }) {
    try {
      console.log(item)
      let { data } = await axios.get('pix/units_of_measure/').catch(r => {
        commit('errors', r.response.data.errors)
      })
      commit('item', { data, item })
    } catch (err) {
      console.error(err)
    }
  },
  clearErrors({ commit }, { model }) {
    commit('clearErrors', { model })
  }
}
export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions,
}
