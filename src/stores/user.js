import axios from 'axios'
export default {
  namespaced: true,
  // -- [ State ] -------------------------------------
  state: {
    token: null,
    user: null,
    reportToken: null,
    selectedCompany: null,
    companies:{},
    loginState:'initial',
  },
  // -- [ Getters ] -------------------------------------
  getters: {
    userBelongsToGroup: state => (role) => {
      return (
        state.user.groups.filter(e => {
          return e.name == role;
        }).length > 0
      )
    }
  },
  // -- [ Mutation ] -------------------------------------
  mutations: {
    setUserToken(state, token) {
      state.token = token
      axios.defaults.headers.common['Authorization'] = `Bearer ${token}`
    },
    removeToken(state) {
      localStorage.removeItem('access_token')
      state.token = null
      state.user = null
      axios.defaults.headers.common['Authorization'] = null
    },
    setUserInfo(cx, user) {
      let c = {}
      user.companies.forEach(e => { c[e.parasql_id] = {name: e.name, prefix: e.prefix}})
      cx.selectedCompany = user.company_id
      cx.companies = c
      cx.user = user
    },
    saveReportSessionToken(state, token) {
      state.reportToken = token
    },
    setCompanyId(state, company_id) {
      state.user.company_id = company_id
    },
    setLoginState(s, state){
      s.loginState = state
    },
  },
  // -- [ Actions ] -------------------------------------
  actions: {
    setCompany({ commit, state }, company_id) {
      let shouldUpdateInServer = company_id && state.user.company_id !== company_id
      commit('setCompanyId', company_id)
      if (shouldUpdateInServer) {
        return axios.post('/api/change_default_company', { company_id }).then(res => {
          let user = res.data
          commit('setUserInfo', user)
        })
      }
      return Promise.resolve('ok')
    },
    createNewUser(_, credentials) {
      return new Promise((resolve, reject) => {
        axios.post('/api/register', {
          name: credentials.name,
          email: credentials.email,
          password: credentials.password,
          password_confirmation: credentials.password_confirmation
        }).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    retrieveToken({commit}, credentials) {
      return new Promise((resolve, reject) => {
        axios.post('/api/login', {
          email: credentials.email,
          password: credentials.password
        }).then(response => {
          if (response.data.user != null) {
            let user = response.data.user
            let token = response.data.user.api_token
            localStorage.setItem('access_token', token)
            commit('setUserToken', token)
            commit('setUserInfo', user)
            resolve(response)
          } else {
            reject(response)
          }
        }).catch(error => {
          reject(error)
        })
      })
    },
    destroyToken({commit, state}) {
      if (state.token != null) {
        let response = new Promise((resolve, reject) => {
          axios.post('/api/logout').then(response => {
            commit('removeToken')
            resolve(response)
          }).catch(error => {
            commit('removeToken')
            reject(error)
          })
        })
        commit('removeToken')
        return response
      }
    },
    swapUser({commit}, user) {
      localStorage.setItem('access_token', user.api_token)
      commit('setUserToken', user.api_token)
      commit('setUserInfo', user)
    },
    getUserInfo({commit}) {
      return new Promise((resolve, reject) => {
        axios.get('/api/user').then(response => {
          let user = response.data.user
          commit('setUserInfo', user)
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    getUserReportSessionToken({ commit }, company_id) {
      return new Promise((resolve, reject) => {
        axios.get(`/api/user/report_token?company_id=${company_id}`).then(response => {
          commit('saveReportSessionToken', response.data.token)
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    initUserStoredState({commit, state, dispatch}) {
      let token = localStorage.access_token
      if (token != null) {
        commit('setUserToken', token)
      }
      return new Promise((resolve, reject) => {
        if (state.token == null) {
          commit('setLoginState','no-token')
        } else {
          dispatch('getUserInfo').then(response => {
            resolve(response)
          }).catch(error => {
            commit('setLoginState','invalid-token')
            reject(error)
          })
        }
        // -----------
      })
    }
  }
}
