import axios from 'axios'
import { get } from 'lodash'
import { nest } from 'd3-collection';

// -- [ State ] -------------------------------------
let state = {
  current: [],
  errors: null,
  company_id: null,
}

// -- [ Getters ] -------------------------------------
let getters = {
  companyItems: (state) => {
    return (id) => {
      return get(state.items, id, [])
    }
  },
  item: (state) => {
    return (id) => {
      return get(state.item, id, {})
    }
  },
}
// -- [ Mutations ] -------------------------------------
let mutations = {
  clearErrors(state, { model }) {
    state.errors[model] = null
  },
  errors(state, { model, errors }) {
    state.errors[model] = errors
  },
  dictionary(state, { id, value }) {
    state[id] = value
  },
  setInventory(state, inventory) {
    state.current = inventory
  },
  setCompanyId(s, c){
    s.company_id = c
  }
}

// -- [ Actions ] -------------------------------------
let actions = {
  getInitialInventory({ state, dispatch }, company) {
    if (state.current.length === 0) {
      dispatch('getInventory', company)
    }
  },
  getInventory({ commit }, company) {
    if (!company) { return null }

    axios.get('/api/inventory/check_inventory', {
      params: {
        company_id: company,
      }
    })
      .then(response => {
        let preparedParts = get(response, 'data', []).map(e => {
          return {
            ...e,
            qty: parseFloat(e.qty == null ? 0 : e.qty)
          };
        });

        let nestedParts = nest()
          .key(d => d.part)
          .rollup(d => {
            return {
              part: d[0].part,
              description: d[0].description,
              part_type: d[0].part_type,
              manufacturer: d[0].manufacturer,
              unit_of_measure_us: d[0].unit_of_measure_us,
              default_location: d[0].default_location,
              total_inventory: d.map(i => i.qty).reduce((p, c) => p + c),
              inventory_by_location: d.map(e => {
                return {
                  location: e.location,
                  qty: e.qty
                };
              })
            };
          })
          .object(preparedParts);

        commit('setInventory', Object.keys(nestedParts).map(e => nestedParts[e]))
        commit('setCompanyId', company)
      })
      .catch(e => {
        console.log('getInventory error')
        console.error(e)
      })
      .finally(() => {
        this.partsAreLoading = false;
      });
  }
}
export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions,
}
