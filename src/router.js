import Vue from 'vue'
import Router from 'vue-router'
import { loadView } from '@/libs/loadView.js'

let router = new Router({
  routes: [

    {
      path: '/icons',
      name: 'IconCatalog',
      component: loadView('views/IconCatalog'),
    },
    {
      path: '/',
      name: 'home',
      component: loadView('views/Home'),
      children: [
        {
          path: '/',
          component: loadView('views/SelectCompany'),
        },
        {
          path: '/:company_id',
          component: loadView('views/Passthrough'),
          children: [
            {
              path: '/',
              component: loadView('views/CurrentInventory'),
            },
            {
              path: 'add_inventory',
              component: loadView('views/CurrentInventory'),
            },
            {
              path: 'move_inventory',
              component: loadView('views/MoveInventoryOptions'),
            },
          ]
        },
      ]
    },
    {
      path: '/login',
      name: 'home',
      component: loadView('views/Home'),
      meta: {
        requiresAuth: true,
        title: 'Home',
      }
    },
    {
      path: '/recover_password',
      name: 'recoverPassword',
      component: loadView('views/RecoverPassword'),
      meta: {
        middleware: [],
        title: 'Password recover',
      }
    },
    {
      path: '/inventory',
      name: 'inventory_transactions',
      component: loadView('views/MaterialTransactions/InventoryManager'),
      meta: {
        showInMenu: true,
        isBeta: true,
        description: 'Check inventory and make movements',
        requiresAuth: true,
        title: 'Inventory',
        allowedGroups: [
          'pix.admin',
          'inventory.clerk',
          'inventory.personnel',
          'inventory.warehouse_manager'
        ]
      }
    },

    {
      path: '*',
      name: 'page_not_found',
      component: loadView('views/PageNotFound'),
      meta: {
        title: 'Page not found',
        middleware: [],
      }
    },
  ]
})
Vue.use(Router)

export default router
