export function search(subject, accessor, search) {
  return search === '' ? subject : subject.filter(option => {
    return (accessor ? option[accessor] : option) ? (
      (accessor ? option[accessor] : option)
        .toString()
        .toLowerCase()
        .indexOf(search.toLowerCase()) >= 0
    ) : false
  })
}
