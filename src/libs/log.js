export function log() {
  return process.env.NODE_ENV === 'production' 
    ? null 
    : console.error(...arguments)
  
}
