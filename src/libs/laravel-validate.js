import { isArray, isString } from 'lodash'

let validations = {
    accepted(value) {
        return /yes|on|1|true/.test(value)
    },

    email(value) {
        // From https://html.spec.whatwg.org/multipage/forms.html#valid-e-mail-address
        // Retrieved 2014-01-14
        // If you have a problem with this implementation, report a bug against the above spec
        // Or use custom methods to implement your own email validation
        return /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/.test(value);
    },

    url(value) {
        return /^(?:(?:(?:https?|ftp):)?\/\/)(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})).?)(?::\d{2,5})?(?:[/?#]\S*)?$/i.test(value);
    },

    date(value) {
        return !/Invalid|NaN/.test(new Date(value).toString());
    },

    dateISO(value) {
        return /^\d{4}[/-](0?[1-9]|1[012])[/-](0?[1-9]|[12][0-9]|3[01])$/.test(value);
    },

    number(value) {
        return /^(?:-?\d+|-?\d{1,3}(?:,\d{3})+)?(?:\.\d+)?$/.test(value)
    },

    digits(value) {
        return /^\d+$/.test(value)
    },

    min(value, min) {
        return value >= min
    },

    max(value, max) {
        return value <= max
    },
    greater(value, min) {
        return value > min
    },

    lower(value, max) {
        return value < max
    },

    minlength(value, min) {
        let length = isArray(value) ? value.length : value.length
        return length >= min
    },

    maxlength(value, max) {
        let length = isArray(value) ? value.length : value.length
        return length <= max
    },

    rangelength(value, min, max) {
        let t = value.length
        return (t >= min && t <= max)
    },

    /**
     * Check if the given value lenght is of the given size
     * @param {string} value Value under validation
     * @param {string} size Size of the string or array
     */
    size(value, size) {
        return (value.length == +size)
    },

    /**
     * Check if a given numerical value is in the range of min and max
     * @param {string} value Value under validation, will be casted to number
     * @param {string} min Min value
     * @param {string} max Max value
     */
    range(value, min, max) {
        let t = +value
        return (t >= +min && t <= +max)
    },

    /**
     * Check if the value provided exists in the list
     * @param {string} value Value under validation
     * @param  {string[]} options Lis of values to check in
     */
    in(value, ...options) {
        return options.includes(value)
    },

    /**
     * Check if the provided date is after a given date
     * @param {string} value Value under validation
     * @param {string} dateString String representing a date in the form YYYY-MM-DD
     */
    after(){
        return true
    },
    
    /**
     * Check if the date provided is on or after a given date
     * @param {string} value Value under validation
     * @param {string} dateString String representing a date in the form YYYY-MM-DD
     */
    after_or_equal(){},

    alpha(value){
        return /^[a-zA-z]+$/.test(value)
    },

    alpha_dash(value){
        return /^[a-zA-Z_-]+$/.test(value)
    },

    /**
     * The field under validation must be entirely alpha-numeric characters
     * @param {any} value Value under validation
     */
    alpha_num(value){
        return /^[a-zA-Z0-9]+$/.test(value)
    },

    /**
     * Check if the value is an `Array`
     * @param {any} value Value under validation
     */
    array(value) {
        return Array.isArray(value)
    },

    /**
     * Stop running validation rules after the first validation failure
     * 
     * == Not implemented yet ==
     * @param {string} value Value under validation
     */
    bail() {
        return true
    },

    boolean(value) {
        return /yes|on|1|true|off|no|false|0/.test(value)
    }

}

let language = {
    en: {
        accepted() { return 'You should accept this field' },
        email() { return 'Please enter your email address' },
        url() { return 'Please enter a valid url e.g. https://xyz.com' },
        date() { return 'Please enter a valid date, e.g. yyyy-mm-dd' },
        dateISO() { return 'Please enter a valid date in ISO format, e.g. YYYY-MM-DD' },
        number() { return 'Please enter a valid number' },
        digits() { return 'Please enter numbers only' },
        minlength(v, min) { return `Please enter more than ${min} characters` },
        maxlength(v, max) { return `Please enter less than ${max} characters` },
        min(v, min) { return `Value should be at least ${min} ` },
        max(v, max) { return `Value should be greater than ${max} ` },
        greater(v, min) { return `Value should be at least ${min} ` },
        lower(v, max) { return `Value should be lower than ${max} ` },
        rangelength(v, min, max) { return `Please enter between ${min} and ${max} characters, currently ${v.length}` },
        size(v, size) { return `Please enter ${size} charactes, currently ${v.length}` },
        range(v, min, max) { return `Please enter a value from ${min} to ${max} inclusive` },
        in(v, ...o) { return `Please enter some of the allowes values, like: ${o.slice(0, 2).concat(', ')}...` }
    },
    es: {
        accepted() { return 'Debes de aceptar este campo para continuar' },
        email() { return 'Por favor ingresa un correo electronico, ej. ejemplo@dominio.com' },
        url() { return 'Por favor ingresa una direccion valida, ej. https://xyz.com' },
        date() { return 'Por favor ingresa una fecha, ej. yyyy-mm-dd' },
        dateISO() { return 'Por favor ingresa una fecha valida, e.g. YYYY-MM-DD' },
        number() { return 'Por favor ingresa un numero valido' },
        digits() { return 'Por favor ingresa solo numeros' },
        minlength(v, min) { return `Ingresa mas de ${min} caracteres, llevas: ${v.length}` },
        maxlength(v, max) { return `Ingresa mas de ${max} caracteres, llevas: ${v.length}` },
        min(v, min) { return `El valor tiene que ser por lo menos ${min}`},
        max(v, max) { return `El valor tiene que ser menor que ${max}`},
        rangelength(v, min, max) { return `Ingresa entre ${min} y ${max} characters, llevas ${v.length}` },
        size(v, size) { return `Ingresa entre ${size} charactes, llevas ${v.length}` },
        range(v, min, max) { return `Ingresa un valor entre ${min} y ${max}` },
        in(v, ...o) { return `Ingresa un valor de la lista: ${o.slice(0, 2).join(', ')}...` }
    }
}

/**
 * Returns if there are any errors in the validations
 * @param {object} validateResponse The result of `validate`
 */
export const fails = (validateResponse) => {
    return Object.keys(validateResponse).map(x => validateResponse[x]).flat().length === 0
}

/**
 * 
 * @param {object} validateResponse The result of `validate`
 * @param {string} key The key you need the errors for
 * @returns {object}
 */
export const get = (validateResponse, key) => {
    return validateResponse[key]
}

/**
 * 
 * @param {object} validateResponse The result of `validate`
 * @param {string} key the key you need to know if there are errors for
 */
export const has = (validateResponse, key) => {
    return validateResponse[key].length === 0
}

/**
 * Validates a set of values according to a set of rules
 * 
 * results in a given language
 * @param {object} rules 
 * @param {object} target 
 * @param {string} lang 
 * @returns {object} The validation errors encountered
 */
export const validate = (rules, target, lang = 'en') => {
    let ans = {}
    let validationList = Object.keys(validations)
    Object.keys(rules).forEach(key => {
        ans[key] = (isString(rules[key]) ? rules[key].split('|') : rules[key])
            .map(t => {
                let [x, y] = /:/.test(t) ? t.split(':') : [t]
                let z = !isString(y) ? [] : y.split(',')
                if (!validationList.includes(x)) { return true }
                return validations[x].apply(
                    null, 
                    [target[key], ...(z)]) ? true : language[lang][x]
                        .apply(null, [target[key], ...z])
            }).filter(x => {
                return isString(x)
            })
    })

    return ans
}