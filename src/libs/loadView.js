export function loadView(view) {
    /*
At `component: () => 'import'`
We can define the folowing variables, separated by "," commas
- `webpackChunkName` This will be the name of the file generated that will contains all the information of this component, this is the key for route level file splitting. This generates a separate chunk (`webpackChunkName`.[hash].js) for this route which is lazy-loaded when the route is visited. In production this route will be cached by the serviceWorker, and will be downloaded only once even
- `webpackPrefetch` This value will tells webpack that this file file should be integrated with the main bundle.
*/
    return () => import(`@/${view}.vue`)
}
