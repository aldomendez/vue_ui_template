import { memoize, get } from 'lodash'

const _parse = (w) => {
    let t = w.split('')
    let s = 'string'
    var pattern = ''
    let replacers = []
    let acc = ''
    // var tik = 0
    let l = []
    let n = 0
    var rep = ''
    var rs = () => Math.random().toString(36).substring(4, 8)
    for (let k = 0; k < t.length; k++) {
        const p = t[k];
        switch (p) {
            case '{':
                s = 'variable'
                rep = '--' + rs()
                acc += rep
                l = []
                l[0] = rep
                pattern += acc
                // tik++
                n = 1
                acc = ''
                break;

            case '.':
                if(s === 'variable'){
                    l[n] = acc
                    n++
                    acc = ''
                } else {
                    acc += p
                }
                break;

            case '}':
                s='string'
                l[n] = acc
                replacers = [...replacers, [...l]]
                break;

            default:
                acc += p
                break;
        }
    }
    return { pattern, replacers }
}

const parse = memoize(_parse)

const replace = (model, data) => {
    let s = model.pattern
    model.replacers.forEach(el => {
        s = s.replace(el[0], get(data, el.slice(1).join('.'), '-'))
    });
    return s
}

export { parse, replace }