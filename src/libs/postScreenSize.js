import {debounce} from 'lodash'
import router from '@/router'
import axios from 'axios'
// https://ryanve.com/lab/dimensions/
// https://stackoverflow.com/questions/504052/determining-position-of-the-browser-window-in-javascript
export default debounce(() => {
  axios.post('api/stats/screen_size',{
    // screen size
    route:router.history.current.fullPath,
    width:window.screen.width,
    height:window.screen.height,
    // window position
    x:window.screenX||window.screenLeft,
    y:window.screenY||window.screenTop,

    innerHeight:window.innerHeight,
    outerHeight:window.outerHeight,
    innerWidth:window.innerWidth,
    outerWidth:window.outerWidth,
  })
}, 1000)
