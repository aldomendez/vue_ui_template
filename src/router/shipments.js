import { loadView } from '@/libs/loadView.js'

export default [
  {
    path: '/shipments',
    component: loadView('views/shipments/Index'),
    meta: {
      requiresAuth: true,
      showInMenu: true,
      isBeta: true,
      title: 'Shipments cockpit',
      description: 'Manage shipment status in realtime',
      allowedGroups: [
        'pix.admin',
        'shipments.reviewer',
        'shipments.manager',
      ]
    },
    children: [
      {
        path: '/',
        name: 'shipment_administration',
        component: loadView('views/shipments/Shipments'),
        meta: {
          requiresAuth: true,
          showInMenu: false,
          isBeta: true,
          title: 'Shipment administration',
          description: 'Manage shipments',
          allowedGroups: [
            'pix.admin',
            'shipments.reviewer',
            'shipments.manager',
          ]
        },
      },
      {
        path: 'broker_agency',
        name: 'broker_agency_admin_panel',
        component: loadView('views/shipments/BrokerAgency'),
        meta: {
          requiresAuth: true,
          showInMenu: false,
          isBeta: true,
          title: 'Broker admin',
          description: 'Manage brokers',
          allowedGroups: [
            'pix.admin',
            'shipments.manager',
          ]
        },
      },
      {
        path: 'driver',
        name: 'driver_admin_panel',
        component: loadView('views/shipments/Driver'),
        meta: {
          requiresAuth: true,
          showInMenu: false,
          isBeta: true,
          title: 'Driver admin',
          description: 'Manage driver',
          allowedGroups: [
            'pix.admin',
            'shipments.manager',
          ]
        },
      },
      {
        path: 'freight_box',
        name: 'freight_box_admin_panel',
        component: loadView('views/shipments/FreightBox'),
        meta: {
          requiresAuth: true,
          showInMenu: false,
          isBeta: true,
          title: 'Freight Box admin',
          description: 'Manage freight boxes',
          allowedGroups: [
            'pix.admin',
            'shipments.manager',
          ]
        },
      },
      {
        path: 'carrier',
        name: 'carrier_admin_panel',
        component: loadView('views/shipments/Carrier'),
        meta: {
          requiresAuth: true,
          showInMenu: false,
          isBeta: true,
          title: 'Carrier admin',
          description: 'Manage carriers',
          allowedGroups: [
            'pix.admin',
            'shipments.manager',
          ]
        },
      },
      {
        path: 'invoice_type',
        name: 'invoice_type_admin_panel',
        component: loadView('views/shipments/InvoiceType'),
        meta: {
          requiresAuth: true,
          showInMenu: false,
          isBeta: true,
          title: 'Invoice Type admin',
          description: 'Manage invoice types',
          allowedGroups: [
            'pix.admin',
            'shipments.manager',
          ]
        },
      },
    ],
  },
  {
    path: '/shipments-dashboard',
    component: loadView('views/shipments/ShipmentsDashboard'),
    meta: {
      requiresAuth: false,
      showInMenu: true,
      isBeta: true,
      title: 'Shipments Dashboard',
      description: 'Show shipments status in realtime',
      allowedGroups: [
        'pix.admin',
        'shipment.dashboard'
      ]
    },
  },
]