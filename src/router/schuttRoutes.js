import { loadView } from '@/libs/loadView.js'

export default [
  {
    path: '/schutts/inventory',
    name: 'schutts_inventory_manager',
    component: loadView('views/Schutts/InventoryManager'),
    meta: {
      showInMenu: true,
      isBeta: false,
      description: 'Control de inventarios de almacen',
      requiresAuth: true,
      title: 'Schutts Inventory',
      allowedGroups: [
        'pix.admin',
        'schutts_warehouse.clerk',
        'schutts_warehouse.personel',
        'schutts_warehouse.warehouse_manager'
      ]
    }
  },
  {
    path: '/shutts/paint/capture',
    name: 'shutts_paint_capture',
    component: loadView('views/Schutts/paint/capture'),
    meta: {
      showInMenu: true,
      isBeta: true,
      description: 'Captura de proporciones de pintura',
      requiresAuth: true,
      title: 'Capture paint proportions',
      allowedGroups: [
        'pix.admin',
        'shutts.paint.capture',
      ]
    },
  },
  {
    path: '/shutts/paint/entry',
    name: 'shutts_paint_job_entry',
    component: loadView('views/Schutts/paint/PaintJobEntry'),
    meta: {
      showInMenu: true,
      isBeta: true,
      description: 'Registros de uso de pinturas',
      requiresAuth: true,
      title: 'Registro de uso de pinturas',
      allowedGroups: [
        'pix.admin',
        'shutts.paint.job.entry',
      ]
    },
  },
]