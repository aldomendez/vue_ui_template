import { loadView } from '@/libs/loadView.js'

export default [
  {
    path: '/powell/create',
    name: 'powell_create_request',
    component: loadView('views/powell/Request'),
    meta: {
      requiresAuth: true,
      title: 'Create request',
      allowedGroups: [
        'powell.supervisor',
        'powell.planner',
        'powell.import-export',
        'powell.engineering',
        'powell.production',
        'powell.quality',
        'powell.internal',
      ]
    },
  },
  {
    path: '/powell/division',
    name: 'powell_division',
    component: loadView('views/powell/Division'),
    meta: {
      // requiresAuth: true,
      title: 'Admin Divisions',
      allowedGroups: [
        'powell.supervisor',
        'powell.planner',
      ]
    },
  },
  {
    path: '/powell/review/:id',
    name: 'powell_review_id',
    component: loadView('views/powell/RequestReview'),
    meta: {
      requiresAuth: true,
      title: 'Review request',
      allowedGroups: [
        'powell.supervisor',
        'powell.planner',
        'powell.import-export',
        'powell.engineering',
        'powell.production',
        'powell.quality',
        'powell.internal',
      ]
    },
  },
  {
    path: '/powell/status/open',
    name: 'powell_request_open',
    component: loadView('views/powell/RequestOpen'),
    meta: {
      showInMenu:true,
      isBeta: true,
      description: 'Powell Shelter Operations, Corporate',
      requiresAuth: true,
      title: 'Powell',
      allowedGroups: [
        'powell.supervisor',
        'powell.planner',
        'powell.import-export',
        'powell.engineering',
        'powell.production',
        'powell.quality',
        'powell.internal',
      ]
    },
  },
]