import { loadView } from '@/libs/loadView.js'

export default [
  {
    path: '/visualhelp',
    name: 'visual_helper_visualizer',
    component: loadView('views/visualHelp/user'),
    meta: {
      showInMenu: true,
      isBeta: true,
      title: 'Visual Help',
      description: 'Show visual help',
      requiresAuth: true,
      allowedGroups: [
        'pix.admin',
        'visualHelp.user',
        'visualHelp.admin',
      ]
    }
  },
  {
    path: '/visualhelp/manager',
    name: 'visual_helper_manager',
    component: loadView('views/visualHelp/manager'),
    meta: {
      showInMenu: false,
      isBeta: true,
      title: 'Visual Help Manager',
      description: 'Manages visual help',
      requiresAuth: true,
      allowedGroups: [
        'pix.admin',
        'visualHelp.admin',
      ]
    }
  },
]