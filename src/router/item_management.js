import { loadView } from '@/libs/loadView.js'

export default [
  {
    path: '/item_management',
    component: loadView('views/item_management/index'),
    meta: {
      requiresAuth: true,
      showInMenu: true,
      isBeta: true,
      title: 'Items management',
      description: 'Manage items and its components',
      allowedGroups: [
        'pix.admin',
        'item.reviewer',
        'item.manager',
      ]
    },
    children: [
      {
        path: ':client_id',
        name: 'list_of_items_for_company',
        component: loadView('views/item_management/ListPartsFromCompany'),
        meta: {
          requiresAuth: true,
          showInMenu: false,
          isBeta: true,
          title: 'Items management',
          description: 'Manage items and its components',
          allowedGroups: [
            'pix.admin',
            'item.reviewer',
            'item.manager',
          ]
        },
      },
      {
        path: '',
        name: 'SelectCompany',
        component: loadView('views/item_management/SelectCompanyFromList'),
        meta: {
          requiresAuth: true,
          showInMenu: false,
          isBeta: true,
          title: 'Items management',
          description: 'Manage items and its components',
          allowedGroups: [
            'pix.admin',
            'item.reviewer',
            'item.manager',
          ]
        },
      },
      {
        path: 'item/:item_id',
        name: 'item_bom',
        component: loadView('views/item_management/ItemBom'),
        meta: {
          requiresAuth: true,
          showInMenu: false,
          isBeta: true,
          title: 'Items management',
          description: 'Manage items and its components',
          allowedGroups: [
            'pix.admin',
            'item.reviewer',
            'item.manager',
          ]
        },
      },
    ],
  },
]