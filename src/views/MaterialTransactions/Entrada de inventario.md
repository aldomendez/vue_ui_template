# Informacion relavante de la entrada de inventario.


## Actividades automaticas de ParaSQL

La siguiente seccion documenta las partes de parasql que estan automatizadas por medio
de triggers y que ayudan a que ciertas partes de ParaSQl funcionen de manera adecuada
al implementar la captura de cierta informacion de la reciba de material se tiene que 
considerar esta magia para que la aplicacion de ParaSQl no se quede con datos incompletos
o basura.

Actualmente se cuenta con la validacion de lo que se recibio de la bodega por 
parte de almacen, y se tiene que se tiene que user una parte de PIX para 
validar que lo que se recibió es lo correcto.

Para evitar que el trabajo se haga doble estare usando la informacion que viene
de esa parte de PIX para jalar la información.

La tabla donde se tiene esa informacion es `QTY_CHECK`

Esa es la definicion de la tabla, basicamente lo unico que usaremos de aqui es la cantidad
y el ID de la partida que complementa, el resto de la informacion es para que ParaSQL funcione correctamente
ya que es dificil hacer que jale informacion de tablas relacionadas en un formulario, se opto por copiar 
informacion por medio de un trigger para no tener ese problema al momento de crear el formulario 
de ParaSQL, es importante mantener este comportamiento por el tiempo de vida de ParaSQL pero no mas
alla, ya que se considera mala practica.

```sql
CREATE TABLE "QTY_CHECK" (
  "QTY_CHECK_ID" bigint(20) NOT NULL COMMENT 'AUTOKEY',
  "ID" bigint(20) DEFAULT NULL COMMENT 'AUTOKEY',
  "QTY" double DEFAULT NULL,
  "QTY_CHECK" tinyint(1) DEFAULT NULL,
  "LOCATION" varchar(30) DEFAULT NULL,
  "USER" varchar(30) DEFAULT NULL,
  "QTY_DATE" datetime DEFAULT NULL,
  "U_M" varchar(30) DEFAULT NULL,
  "NOTIFIED" tinyint(1) DEFAULT NULL,
  PRIMARY KEY ("QTY_CHECK_ID"),
  KEY "ID" ("ID"),
  CONSTRAINT "QTY_CHECK_ibfk_1" FOREIGN KEY ("ID") REFERENCES "QTY" ("ID") ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
```

La informacion que se requiere para que esto funcione esta como un trigger de la tabla `TRANSACTION_TABLE`
basicamente cuando se cambia la transaccion del estado en la que aparace con Import/Export, a el estado
de que Import/Export ya la libero, se agrega reescribe en la tabla de `TRANS_REC_MS` los datos de las
partidas que luego tendran que ser capturados por el personal.

```sql
CREATE PROCEDURE "insert_trans_rec_ms_tracking_data"(IN trans bigint(20))
begin
    -- clear database entries
    delete from TRANS_REC_MS where TRANSACTION_ID = trans;
    -- insert new values
    insert into TRANS_REC_MS (ID, DATE_SEND, STATUS,TRANSACTION_ID,LOAD_CHECK)
    SELECT db11423.parasql_next_counter_value('TRANS_REC_MS', 'ID') ID, CURRENT_TIMESTAMP(),'INTRANSIT',trans,0;

end
```

Por su lado cuando se ejecuta la insercion en la tabla de `TRANS_REC_MS` se ejecuta otro trigger, mostrado
a continuacion, que basicamente inserta en QTY_CHECK informacion de las partidas con valores base que sirven para generar el rastreo de la informacion. Este query a su vez completa la transaccion cuando se cambia para que se llene la

```sql
BEGIN

	IF (OLD.LOAD_CHECK is null or OLD.LOAD_CHECK = 0) and NEW.LOAD_CHECK = 1 THEN 
		insert into QTY_CHECK (QTY_CHECK_ID, ID, QTY, QTY_CHECK, LOCATION, USER, QTY_DATE, U_M)
		select parasql_next_counter_value('QTY_CHECK','QTY_CHECK_ID'),
		ID, 0, 0, 'MS-1','',current_timestamp(), (select UM_US from PART p where p.PART = q.PART)
		from QTY q where q.TRANSACTION_ID = NEW.TRANSACTION_ID;
		update TRANSACTION_TABLE set TRANS_MS = 1 where TRANSACTION_TABLE_ID = NEW.TRANSACTION_ID;
	END IF;

	IF OLD.LOAD_CHECK = 1 and (NEW.LOAD_CHECK is null or NEW.LOAD_CHECK = 0) THEN 
		delete from QTY_CHECK where ID in (select ID from QTY where TRANSACTION_ID = NEW.TRANSACTION_ID);
	END IF;
END
```

Esto cubre la parte de ParaSQL que pasa detras de camaras, por el momento no se puede presindir de esta parte
del proceso por que todos los clientes que reciben sus transacciones por parte de la aplicacion de `Almacen` 
hacen uso de todas estas partes que alimentan el sistema.

Ademas para mantener compatibilidad, la aplicacion tiene que hacerse cargo de alimentar hacia ParaSQL,
para que no se pierda la informacion.

## Cosas que se tienen que considerar al armar esta aplicacion

- Que es lo que va a pasar cuando querramos ingresar material que aun no esta en la base de datos de numeros de parte
  que tenemos de el inventario de numeros de parte
    - Basado en que se tiene informacion de PIX, para el numero de parte en ParaSQL, se podra importar de manera
      transparente cuando de haga la importacion de material.
    - Si es asi, quien seria la persona responsable de poner la 
    - La tabla de informacion de los numeros de parte solo contiene informacion basica de los numeros de parte
      por lo que se tiene que poner el numeros de parte como incompleto? se tiene que notificar a quien este a cargo
      de la integridad de los numeros de parte para que lo revise y continue con la informacion que se
      tiene pendiente.
- La informacion que se tiene en inventario que entro desde mexico, Llevara algun tipo de informacion que
  avale/compruebe que llego aqui de manera legal. Pix tiene que guardar una copia de la informacion/papeleria?


## Consideraciones de la implementacion

Para que la implementacion funcione, se tiene que tener el rastreo de la transaccion de la que viene
el registro.



## Opciones de llenado para el Modelo `MaterialRequisition`


La intension de documentar esta parte de la aplicacion es que este modelo, tendra 
diferente informacion dependiendo de varias situaciones:

- Entrada de material al almacen
- Salida de material al almacen
- Movimiento de material dentro del almacen
- Retorno de material desde produccion
- Ajuste de inventario
- Ajuste de una transaccion anterior con errores

Para poder realizar cualquiera de las transacciones anteriores se tiene que proporcionar la 
siguiente informacion, de acuerdo a la transaccion que se este realizando. Con esto aseguramos
que todas las transacciones tienen la informacion adecuada para ser analizadas.

- Reciba de material de la bodega (meter material al almacen)
  - `company_id`: Numero de la compañia utilizando el numero de `parasql.COMPANY.COMPANY_ID`
  - `transaction_id` El numero de la transaccion de donde sale el material `parasql.TRANSACTION_TABLE.TRANSACTION_TABLE_ID`
  - `reason` En caso de que se requiera, se puede poner en este campo una razon, pero en general, esto solo aplica en material entregado
  - `movement_type` [IN] Tipo de movimiento de material que se esta realizando
  - `user_id` id de la persona que esta reciviendo el material.
- Reciba de material de otro lugar
  - `company_id` ID de la compañia a la que se le cargara el inventario
  - `transaction_id` No se requiere por que no hay una transaccion para asignar
  - `reason` Un comentario que indique el por que se esta haciendo este abono de inventario de manera manual
  - `movement_type` [MANUAL_IN] Tipo de movimiento que se esta realizando
  - `user_id` ID de la persona que esta haciendo el movimiento
- Entrega de material (sacer material del almacen)
  - `company_id` Numero de la compañia a la que le pertenece esta entrega de material
  - `cute_ticket` numero de orden de corte a la que esta orden esta surtiendo
  - `reason` En caso de que se requiera, se puede poner en este campo una razon, pero en general, esto solo aplica en material entregado
  - `movement_type` [OUT] Tipo de movimiento de material que se esta realizando 
  - `order_id` Este valor no me acuerdo para que es que quedo definido, pero en cuanto lo recuerde lo anotare aqui
  - `user_id` ID de la persona que esta haciendo el movimiento
  - `interacted_user_id` ID del usuario que recibe el material.
- Movimiento de material dentro del almacen
  - `company_id`
  - `reason` Razon por la que se esta realizando el cambio
  - `movement_type` [MOVE] Tipo de movimiento que se esta realizando, esta transaccion
  deberia de tener movimientos balanceados para que justifique que no ha habido cambios 
  en la cantidad de material que se tiene en almacen. Lo que significa que al terminar de 
  aplicarse el movimiento, este no debera de generar cambios en un numero de parte.
  Es la intension de que solo cambien los balances dentro de las locaciones, ya sea para
  concentrarlas o distribuirlas dentro del almacen.
  - `user_id` ID de la persona que esta haciendo el movimiento.
- Retorno de material desde produccion
  - `company_id` ID de la compañia que esta haciendo el movimiento
  - `cute_ticket` Numero de ticket al que se le estara abonando este material, que se pidio, pero no se uso
  - `movement_type` [PRODUCTION_RETURN] Tipo de movimiento que se esta realizando
  - `user_id` ID de la persona que esta realizando el movimiento
  - `reason` Razon por la que se esta haciendo el retorno de material. En caso de que se comparta esta informacion
  - `interected_user_id` ID de la persona que entrego el material a almacen.
- Ajuste de inventario
  - `company_id` ID de la compañia a la que pertencen los numeros de parte
  - `movement_type` [INVENTORY_ADJUSTMENT] Tipo de movimiento que se esta realizando
  - `reason` Razon por la que se estan haciendo los ajustes de inventario
  - `user_id` ID del usuario que esta realizando la operacion
- Ajuste por una transaccion anterior con errores
  - `company_id` ID de la compañia de la que se estan haciendo los movimientos
  - `movement_type` [MOVEMENT_ADJUSTMENT] Tipo de movimiento que se esta realizando
  - `reason` Nota del por que se esta haciendo el movimiento.
  - `user_id` ID de la persona que esta haciendo el movimiento
  - ``
