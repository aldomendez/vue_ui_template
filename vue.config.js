let glob = require('glob-all')
let PurgecssPlugin = require('purgecss-webpack-plugin')
let path = require('path')

class TailwindExtractor {
  static extract(content) {
    return content.match(/[A-Za-z0-9-_:/]+/g) || []
  }
}

let production = process.env.NODE_ENV === 'production'

module.exports = {
  // proxy API requests to Valet during development
  devServer: {
    proxy: 'http://nova-portal.test/'
  },

  // publicPath: 'https://novalinkpix.com/v4/public/',
  // publicPath: '/',
  publicPath: production ? '/_inventory/' : '/',
  productionSourceMap: false,

  integrity: false,

  pwa: {
    workboxPluginMode: 'GenerateSW',
    skipWaiting: false,
    // exclude: [/.*/]
  },

  // output built static files to Laravel's public dir.
  // note the "build" script in package.json needs to be modified as well.
  outputDir: '../../public/_inventory',

  // modify the location of the generated HTML file.
  // make sure to do this only in production.
  indexPath: production
    ? '../../resources/views/inventory_view.blade.php'
    : 'inventory_view.html',
  // to delete the prefetch from the initial page, so no wasted resources
  // from the client to try to load every file in the site, specially when
  // only one or two pages as being used
  chainWebpack: (config) => {
    // config.plugins.delete('pwa')
    // config.plugins.delete('workbox')
    config.plugins.delete('prefetch')
  },
  css: {
    loaderOptions: {
      sass: {
        // data: '@import "@/variables.scss";'
      }
    }
  },
  configureWebpack: {
    plugins: [
      new PurgecssPlugin({
        whitelistPatterns: [],
        paths: glob.sync([
          path.join(__dirname, './**/*.html'),
          path.join(__dirname, './src/**/*.vue'),
          path.join(__dirname, './src/**/*.js'),
          path.join(__dirname, './node_modules/buefy/src/**/*.vue'),
        ]),
        extractors: [{
          extractor: TailwindExtractor,
          extensions: ['html', 'js', 'vue']
        }]
      }),
    ]
  }
}
